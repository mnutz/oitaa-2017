import machine

pin = machine.Pin(15)

timer = machine.Timer(2)

ch = timer.channel(2, machine.Timer.PWM, pin=pin)

timer.freq(440)

ch.pulse_width_percent(20)

timer.counter(0)

machine.delay(300)