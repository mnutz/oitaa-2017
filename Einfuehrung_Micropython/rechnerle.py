import machine
from machine import Pin
from dht import DHT11
from neopixel import NeoPixel
from ssd1306 import SSD1306_I2C

class Rechnerle:

    def __init__(self):
        pass

    @property
    def display(self):
        if not hasattr(self, '_display') or self._display is None:
            display_i2c = machine.I2C(scl = Pin(5), sda = Pin(4))
            self._display = SSD1306_I2C(128, 64, display_i2c)
        return self._display

    @property
    def dht(self):
        if not hasattr(self, '_dht') or self._dht is None:
            self._dht = DHT11(Pin(14))
        return self._dht

    @property
    def pixels(self):
        if not hasattr(self, '_pixels') or self._pixels is None:
            self._pixels = NeoPixel(Pin(2), 3)
            self._pixels.fill((0, 0, 0))
        return self._pixels
    
    @property
    def analog(self):
        if not hasattr(self, '_analog') or self._analog is None:
            self._analog = machine.ADC(0)
        return self._analog
    
    @property
    def push(self):
        if not hasattr(self, '_push') or self._push is None:
            self._push = Pin(0, Pin.IN, Pin.PULL_UP)
        return not self._push.value()

    @property
    def dreh(self):
        if not hasattr(self, '_dreh') or self._dreh is None:            
            self._dreh = (Pin(12, Pin.IN, Pin.PULL_UP), Pin(13, Pin.IN, Pin.PULL_UP))
        return 2 * self._dreh[0].value() + self._dreh[1].value();

    def tone(self, freq=400, duty=500, on=True):
        if not hasattr(self, '_pwm') or self._pwm is None:
            self._pwm = machine.PWM(Pin(15))
            self._current_pwm = False
        if self._current_pwm != on:
            self._pwm.freq(freq)
            self._pwm.duty(duty)
            if on:
                self._pwm.init()                
            else:
                self._pwm.deinit()
            self._current_pwm = on

if __name__ == "__main__":
    import time
    rechnerle = Rechnerle()
    display = rechnerle.display
    pixels = rechnerle.pixels

    t = time.ticks_ms()
    temp, hum = (0, 0)
    pixel = 0
    analog = 0
    rechnerle.dreh

    while(True):
        display.fill(0)

        analog = rechnerle.analog.read()

        rechnerle.tone(on=rechnerle.push)

        if time.ticks_ms() - t >= 1000:
            t += 1000

            rechnerle.dht.measure()
            temp, hum = (rechnerle.dht.temperature(), rechnerle.dht.humidity())

            # GRB pixel type
            pixels.fill((0, 0, 0))
            pixels[pixel] = (0, 0, 255)
            pixels.write()
            pixel = (pixel + 1) % pixels.n

        #display.text('%d %d' % (rechnerle._dreh[0].value(), rechnerle._dreh[1].value()), 0, 0)
        display.text('Drehgeber: %d' % rechnerle.dreh, 0, 0)
        display.text('Analog: %d' % analog, 0, 10)
        display.text('Temperatur: %d' % temp, 0, 20)
        display.text('Feuchtigkeit: %d' % hum, 0, 30)

        display.show()
        time.sleep_ms(40)