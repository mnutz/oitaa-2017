# Python auf dem ESP8266
Micropython erlaubt eine interaktive Softwareentwicklung in Python auf dem ESP. Python ist geeignet für Einsteiger, um das Programmieren auf einem Arduino zu lernen, aber auch für Fortgeschrittene.

## Python installieren
Zuerst muss auf dem PC eine aktuelle Python-Version installiert sein. Dazu laden wir auf https://www.python.org/downloads/ die aktuellste stabile Version (3.6.4) herunter und installieren diese.
*Achtung: Damit man Python später in jedem Verzeichnis ausführen kann, muss der Haken bei __Add Python to environment variables__ aktiviert sein!*
Danach öffnen wir die Powershell (oder Kommandozeile) und prüfen, ob Python richtig installiert wurde.

    python --version

Die Ausgabe `Python 3.6.4` zeigt an, dass die Installation funktioniert hat. Sollte hier ein Fehler auftreten muss die Installationsdatei erneut ausgeführt werden und unter "Modify" der Haken wie oben beschrieben gesetzt werden.

## Treiber installieren
Um den ESP unter Windows zu verwenden muss der passende Treiber installiert werden.
https://wiki.wemos.cc/downloads

Wenn man jetzt den ESP am Computer anschließt kann man im Gerätemanager unter "Anschlüsse (COM und LPT)" den verwendetet USB-to-Serial Port, z. B. COM3, finden.

## Micropython flashen
Zuerst laden wir die akteulle Version (1.9.3) von Micropython für den ESP8266 herunter.
https://micropython.org/download#esp8266

Nun müssen wir mit dem Python Paketmanager das Werkzeug "esptool" installieren. Dazu öffnen wir die Powershell im Downloadverzeichnis, in dem die Micropython Firmware liegt.
*Im Datei-Explorer Shift + Rechtsklick aktiviert den Eintrag __Powershell hier öffnen__ im Kontextmenü*

    pip3 install esptool

Falls die Zugriffsrechte nicht ausreichen muss eventuell die Powershell als Administrator ausgeführt werden.

Als nächstes müssen wir den Flash des ESP komplett löschen. Dazu führen wir den folgenden Befehl aus.
*Statt "COM3" muss der aktuell verwendete Port angegeben werden!*

    esptool.py --port COM3 erase_flash

Dann schreiben wir das Micropython Image auf den ESP.

    esptool.py --port COM3 -b 460800 write_flash --flash_size=detect --flash_mode=dio 0 esp8266-20171101-v1.9.3.bin

Damit ist Micropython auf dem Gerät aktiv und man kann beginnen.

## Verbinden mit Putty
Mit Putty kann man nun eine Verbindung zum ESP aufbauen. Dazu muss der Verbindungstyp *Serial/Seriell* ausgewählt und der richtige Port eingetragen werden. Als Baudrate (Speed) geben wir 115200 ein.
Wenn die Verbindung aufgebaut ist muss eventuell einmal *Enter* gedrückt werden um die interaktive Python-Shell angezeigt zu bekommen.

    >>>

In der Shell kann man Python verwenden, um mit dem ESP zu kommunizieren.
*Zum Beispiel 2 + 3 rechnen oder die grüne LED im Rechnerle einschalten.*

    >>> 3 + 2
    5
    >>> import machine
    >>> p = machine.Pin(16)
    >>> p.on()
    >>> p.value()
    1

## Dateien auf dem ESP ausführen
Um Dateien auf den ESP zu laden benötigen wir das **A**dafruit **M**icro**Py**thon Tool (ampy). Dieses installieren wir wieder mit dem Python Paketmanager.

    pip3 install adafruit-ampy

Mit diesem Tool kann man nun in der Powershell Dateien auf den ESP kopieren oder sofort ausführen.

    ampy -p COM3 put ssd1306.py
    ampy -p COM3 run -n display.py

## Quellen
  * http://philipp-weissmann.de/blog/python-auf-dem-esp8266/
  * https://docs.micropython.org/en/latest/esp8266/esp8266/tutorial/