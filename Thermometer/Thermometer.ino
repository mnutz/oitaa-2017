#include <Arduino.h>
#include <NeoPixelBus.h>
#include <SimpleDHT.h>
#include "SSD1306.h" // display
#include <Hash.h>

#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <WebSocketsServer.h>

// Temperatur / Luftfeuchtigkeit auf webseite anzeigen
// Automatisch akualisieren

SimpleDHT11 dht11;
SSD1306 display(0x3c, D2, D1);
NeoPixelBus<NeoRgbFeature, NeoEsp8266BitBang400KbpsMethod> strip(3, 2);

int dhtpin = 14;

ESP8266WebServer http(80);
WebSocketsServer websocket(81);

void indexHtml()
{
    static byte temperature = 0, humidity = 0;
    dht11.read(dhtpin, &temperature, &humidity, NULL);

    http.send(200, "text/html", R"=====(<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
    </ead
     <body>
    <h1>Temperatur: <span id="temp"></span>°</h1>
    <h1>Feuchtigkeit: <span id="hum"></span>%</h1>

    <div style="width:75%;">
        <canvas id="canvas"></canvas>
    </div>

    <script src="//cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
    <script>
        var connection = new WebSocket('ws://' + location.hostname + ':81');

        var context = document.getElementById('canvas').getContext('2d');

        var diagramm = new Chart(context, {
            type: 'line',
            data: {
                labels: function() {a=[];for(i=59;i>=0;i--)a.push(-i);return a;}(),
                datasets: [
                    {
                        key: 't',
                        yAxisID: 'T',
                        label: 'Temperatur',
                        borderColor: 'red',
                        data: function() {a=[];for(i=0;i<60;i++)a.push(0);return a;}(),
                        fill: false
                    },
                    {
                        key: 'h',
                        yAxisID: 'F',
                        label: 'Feuchtigkeit',
                        borderColor: 'blue',
                        data: function() {a=[];for(i=0;i<60;i++)a.push(0);return a;}(),
                        fill: false
                    }
                ]
            },
            options: {
                 scales: {
                    yAxes: [{
                        id: 'T',
                        type: 'linear',
                        position: 'left',
                        ticks: {
                            max: 40,
                            min: -10
                        }
                    }, {
                        id: 'F',
                        type: 'linear',
                        position: 'right',
                        ticks: {
                            max: 100,
                            min: 0
                        }
                    }]
                    }   
            }
        });

        function addData(chart, data) {
            console.log(data)
            chart.data.datasets.forEach(
                function(dataset) {
                    dataset.data.shift();
                    dataset.data.push(data[dataset.key]);
                }
            );
            chart.update();
        }

        connection.onmessage = function(event){
            var daten = JSON.parse(event.data);

            document.getElementById('temp').innerText = daten.t;
            document.getElementById('hum').innerText = daten.h;

            addData(diagramm, daten);

        };

    </script>
    </body>
</html>)=====");
}

String getReadableIp(IPAddress ip)
{
    return (String)ip[0] + "." + (String)ip[1] + "." + (String)ip[2] + "." + (String)ip[3];
}

// void websocketEvent(uint8_t num, WStype_t type, uint8_t *payload, size_t length){
//     switch (type)
//     {
//     case WStype_DISCONNECTED:
//         return;
//     case WStype_CONNECTED:
//         return;
//     case WStype_TEXT:
//         return;
//     case WStype_BIN:
//         return;
//     }
// }

void setup()
{
    Serial.begin(115200);

    strip.ClearTo(RgbColor(0, 0, 0));
    strip.Show();

    display.init();
    display.flipScreenVertically();
    display.setFont(ArialMT_Plain_10);

    // WiFi.mode(WIFI_AP);
    // WiFi.softAP("Temperatur");

    WiFi.mode(WIFI_STA);
    WiFi.begin("WIFI_SSID", "WIFI_PASS");
    int d = 60;
    Serial.println("test");
    while (d-- > 0 && WiFi.status() == WL_DISCONNECTED)
        delay(500);
    if (WiFi.status() == WL_CONNECTED)
        display.drawString(0, 0, getReadableIp(WiFi.localIP()));
    display.display();

    http.on("/", indexHtml);

    http.begin();

    websocket.begin();
}

void loop()
{
    http.handleClient();
    websocket.loop();

    static long delta_t = 1000;
    static long t, t1 = millis() - delta_t;
    t = millis();
    if (t - t1 < delta_t)
        return;
    t1 += delta_t;

    static byte temperature = 0, humidity = 0;
    dht11.read(dhtpin, &temperature, &humidity, NULL);
    websocket.broadcastTXT("{ \"t\": " + (String)temperature + ", \"h\": " + (String)humidity + " }");
}