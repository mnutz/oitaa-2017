#include <Arduino.h>
#include <internal/RgbColor.h>

extern String index_html(String text, String color, int speed)
{
    return R"=(<!DOCTYPE html>
<html lang="de-DE">
    <head>
        <title>Laufschrift</title>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no" />
        <style type="text/css">
            #textButton { float: right; }
            #textInput { overflow:hidden; }
            #text, #color, #speed { display:block; width:100%; }
        </style>
    </head>
    <body>
    <h1>Laufschrift</h1>
        <h2>Text</h2>
        <form id="textForm">
            <div id="textButton">
                <input type="submit" value="Ok">
            </div>
            <div id="textInput">
                <input id="text" type="text" value=")=" + text + R"=(" maxlength="100" />
            </div>
        </form>
        <h2>Farbe</h2>
        <div>
            <input id="color" type="color" value=")=" + color + R"=(" />
        </div>
        <h2>Geschwindigkeit</h2>
        <div>
            <input id="speed" type="range" value=")=" + speed + R"=(" min="1" max="20" value="10">
        </div>
        <script type="text/javascript">
            document.querySelector('#textForm').onsubmit = function(event){
                event.preventDefault();
                console.log(event.target);
                api_call('/settext', {text: document.querySelector('#text').value});
            };

            document.querySelector('#color').addEventListener('change', function(event){
                api_call('/setcolor', {color: event.target.value});
            });

            document.querySelector('#speed').addEventListener('input', function(event){
                api_call('/setspeed', {speed: event.target.value});
            });

            function api_call(url, params){
                var request = new XMLHttpRequest();
                request.open('POST', url + toParamList(params));
                request.addEventListener('load', function(event){
                    if(request.status >= 200 && request.status < 300)
                        return;
                    else
                        alert(request.statusText);                             
                });
                request.send();
            }
            function toParamList(obj){
                var str = '?';
                for(var key in obj){
                    if(str !== '')
                        str += '&';
                    str += key + '=' + encodeURIComponent(obj[key]);
                }
                return str;
            }
        </script>
    </body>
</html>
)=";
}